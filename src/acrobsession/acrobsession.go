package main

import (
	"github.com/go-martini/martini"
	"github.com/martini-contrib/render"
	"net/http"
	"upper.io/db"
	"upper.io/db/postgresql"
	"log"
	"acrobsession/models"
	"time"
	"net/url"
	"strconv"
	"io/ioutil"
)

func main() {
	var settings = db.Settings{
		Host:     "localhost",
		Database: "acrobsession",
		User:     "postgres",
		Password: "",
	}

	db, err := db.Open(postgresql.Adapter, settings)
	if err != nil {
		log.Fatalf("db.Open(): %q\n", err)
	}
	defer db.Close()

	app := martini.Classic()
	app.Use(render.Renderer(render.Options{
		Layout: "layout",
	}))
	app.Map(db)

	app.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
		responseWriter.Header().Set("Location", "/reports")
		responseWriter.WriteHeader(http.StatusSeeOther)
	})

	app.Post("/reports", postReport)

	app.Get("/reports", reports)

	app.NotFound(func(renderer render.Render) {
		renderer.HTML(404, "404", "")
	})

	app.Run()
}

func reports(renderer render.Render, request *http.Request, session db.Database) {
	renderer.HTML(200, "reports", models.GetReports(session))
}

func postReport (responseWriter http.ResponseWriter, request *http.Request, session db.Database) {
	const maxBodySize = 1 * 1024 * 1024 // 1mb

	reportTable, err := session.Collection("reports")
	if err != nil {
		log.Panicf("session.Collection(): %q\n", err)
	}

	var bodyValue string

	if body, err := ioutil.ReadAll(request.Body); err == nil {
		if len(body) > 0 && maxBodySize != 0 {
			if request.ContentLength < int64(maxBodySize) {
				bodyValue = string(body)
			} else {
				bodyValue = string(body[0:maxBodySize])
			}
		}
	}

	if len(bodyValue) == 0 {
		responseWriter.WriteHeader(http.StatusInternalServerError)
	}

	var id int64
	var tmp interface{}
	tmp, err = reportTable.Append(models.Report{
		CreatedAt: time.Now(),
		Json: bodyValue,
	})
	id = tmp.(int64)

	if err != nil {
		log.Panicf("reportTable.Append(): %q\n", err)
	}

	responseWriter.Header().Set("Location", "/reports/" + url.QueryEscape(strconv.FormatInt(id, 10)))
	responseWriter.WriteHeader(http.StatusCreated)
}
