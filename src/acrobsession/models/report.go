package models

import (
	"time"
	"upper.io/db"
	"log"
)

type Report struct {
	Id int64 `db:"id,omitempty"`
	CreatedAt time.Time `db:"created_at"`
	Json string `db:"json"`

	// from json
	PackageName string `db:"package_name,omitempty"`
	AppVersionName string `db:"app_version_name,omitempty"`
	AppVersionCode string `db:"app_version_code,omitempty"`
	AndroidVersion string `db:"android_version,omitempty"`
	PhoneModel string `db:"phone_model,omitempty"`
	Logcat string `db:"logcat,omitempty"`
	StackTrace string `db:"stack_trace,omitempty"`
	Reason string `db:"reason,omitempty"`
}

func GetReports(session db.Database) []Report {
	reportTable, err := session.Collection("reports")
	if err != nil {
		log.Panicf("session.Collection(): %q\n", err)
	}

	var reports []Report
	result := reportTable.Find().Select(
		`id`,
		`created_at`,
		`json`,
		db.Raw{`json->'PACKAGE_NAME' as package_name`},
		db.Raw{`json->'APP_VERSION_NAME' as app_version_name`},
		db.Raw{`json->'APP_VERSION_CODE' as app_version_code`},
		db.Raw{`json->'ANDROID_VERSION' as android_version`},
		db.Raw{`json->'PHONE_MODEL' as phone_model`},
		db.Raw{`json->'LOGCAT' as logcat`},
		db.Raw{`json->'STACK_TRACE' as stack_trace`},
	)
	err = result.All(&reports)
	if err != nil {
		log.Panicf("res.All(): %q\n", err)
	}

	return reports
}
